# Ultimate Eligibility

Detect inactive users and potential guest users.

This will help you see how much Ultimate may cost you.

#### How To Use

1. Clone or fork repository into your own namespace
1. Go to `CI/CD` > `Pipelines` > `Run Pipeline`
1. Provide a GitLab Instance via the `GITLAB_HOST` and an admin token via `PRIVATE_TOKEN`
1. Voila! The resulting report will tell you how many licenses you may need to purchase if you went to ultimate.