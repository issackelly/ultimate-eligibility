from envparse import Env
import os
from gitlab import Gitlab as GitLab
import sys
import datetime

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def main():
    private_token = env('PRIVATE_TOKEN')
    origin = env('GITLAB_HOST')
    debug = env('DEBUG', cast=bool, default=False)
    gl = GitLab(url=origin, private_token=private_token)
    gl.auth()

    users = gl.users.list(all=True, active=True, obey_rate_limit=False)
    reporter_plus_events = ['approved', 'pushed to', 'pushed new', 'imported', 'accepted', 'merged']

    all_event_actions = set()
    eligible_for_removal = []
    eligble_for_guest = []
    count = 0
    print(f'Running scan for {len(users)} users.')
    print(f"Users with no events will be counted for potentials for removal.")
    print(f'Users with no events in the following list will be counted for potentials for guest access only.')
    print(f' - ', ', '.join(reporter_plus_events))
    sys.stdout.write(f"{count+1}-{count + 10} of {len(users)}: ")
    for user in users:
        count = count + 1
        print("\t ", datetime.datetime.utcnow().isoformat(), "\t", user.username)

        if user.username in ['build-deploy-gitops-prod',]:
            print ("\tskipping the hung user..? ")
            continue

        if count % 10 == 0:
            sys.stdout.flush()
            print()
            sys.stdout.write(f"{count+1}-{count + 10} of {len(users)}: ")

        # Look for any inactive user
        if user.state != 'active':
            print('NOT ACTIVE!', user)
            continue

        if debug and count > 50:
            break

        events = user.events.list(all=True, obey_rate_limit=False, max_retries=2)
        if not events:
            eligible_for_removal.append(user.id)
            continue
        guest = True
        for action in list(set(x.action_name for x in events)):
            all_event_actions.add(action)
            if action in reporter_plus_events:
                guest = False
                break
        if guest:
            eligble_for_guest.append(user.id)
    print()
    print('Eligible for Removal:', len(eligible_for_removal))
    print('Eligible for Guest Access:', len(eligble_for_guest))
    print('Total Number of Ultimate Licenses to Pay for:', len(users)-len(eligble_for_guest)-len(eligible_for_removal))
    print('All events seen:', ', '.join(list(all_event_actions)))



if __name__ == "__main__":
    main()
